<?php 
if(!isset($_SESSION))
{
session_start();
}
include ("../koneksi.php");?>
<html>
<head>
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="jquery-1.9.0.js"> </script>
<script type="text/javascript"> 
var htmlobjek;
$(document).ready(function(){
  $("#idprodi").change(function(){
    var idprodi = $("#idprodi").val();
	var nilaiawal=$("#nilaiawal").val();
	var nilaiakhir=$("#nilaiakhir").val();
    $.ajax({
        url: "laporan_rekap_per_prodi_rentang_adm.php",
        data: "idprodi="+idprodi+"&nilaiawal="+nilaiawal+"&nilaiakhir="+nilaiakhir,
        cache: false,
        success: function(msg){
            //jika data sukses diambil dari server kita tampilkan
            //di <select id=kota>
            $("#laporan").html(msg);
        }
    });
  });
  });
</script>
</head>
<body>
<h3 class="labeljudul" align="center"> REKAP MATA KULIAH YANG AKAN DIAMBIL <br>
PADA SEMESTER KHUSUS THN 2014/2015 </h3>
<?php
echo "<form>";
echo "<table cellpadding='2' >";
echo "<tr><td class='lbltext'>Peserta dari </td><td>:</td> <td><input type='text' id='nilaiawal' name='nilaiawal' size='5'> <b>S.d.</b> <input type='text' name='nilaiakhir' id='nilaiakhir' size='5'> </td> </tr>";
echo "<tr><td class='lbltext'> INPUT PPRODI </td><td>:</td>";
echo "<td ><select id='idprodi' name='idprodi'>";
echo "<option value='0'> -- Pilih Prodi -- </option>";
$a=mysql_query("SELECT * FROM tbl_prodi");
while($b=mysql_fetch_array($a)){
echo "<option value=".$b['id_prodi'].">".$b['nm_prodi']."</option>\n";
}
echo "</select></td></tr>";
echo "</table>";
echo "</form>";
?>
<div id="laporan"></div>
</body>
</html>