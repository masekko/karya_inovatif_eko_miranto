<?php 
include ("../koneksi.php") ?> 
<html>
    <head>
        <title>master matkul</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script   src="../media/js/jquery.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <link rel="StyleSheet" href="css/style.css" type="text/css" />
        <style type="text/css">
            @import "../media/css/demo_table_jui.css";
            @import "../media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
        </style>
        
        <style>
            *{
                font-family: arial;
            }
        </style>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $('#datatables').dataTable({
                    "sPaginationType":"full_numbers",
                    "aaSorting":[[2, "desc"]],
                    "bJQueryUI":true
                });
            })
            
        </script>
    </head>
    <body>
        <table id="datatables" class="display">
                <thead>
                    <tr>
                        <th> NO Urut </th>
						<th>Kode matkul</th>
						<th>Nama matkul</th>
						<th> Matkul Untuk Prodi </th>
                        <th>EDIT || DEL </th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
				$sql="SELECT m.id_matkul, m.nm_matkul, p.id_prodi, p.nm_prodi FROM tbl_matkul AS m, tbl_prodi AS p
				 WHERE m.id_prodi=p.id_prodi ";
				$result = mysql_query($sql) or die(mysql_error());
				$no = 1;
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
							<td> <?php echo $no ?></td>
							<td><?php echo $row['id_matkul']?></td>
                            <td><?php echo $row['nm_matkul']?></td>
							<td><?php echo $row['nm_prodi'] ?></td>
							<!-- update tanpa nama prodi karena tidak ditampilkan dalam dialog box -->
                          <?php echo " <td align='center'>
						  <a href='javascript:void(0)' onClick=\"edit_form('$row[id_matkul]','$row[id_prodi]')\">
						  <img title=\"Edit Data\"  src=\"images/edit.png\"/></a> 
																"."  ||   "."  
						 <a href='javascript:void(0)' onClick=\"delete_data('$row[id_matkul]','$row[id_prodi]','$row[nm_prodi]','$row[nm_matkul]')\">
						 <img title =\"Delete Data\" src=\"images/delete.jpg\" /></a></td> " ?>
                        </tr>
                        <?php
						$no++;
                    }
                    ?>
                </tbody>
            </table>
</body>
</html>
