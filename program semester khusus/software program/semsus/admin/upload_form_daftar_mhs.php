<html>
<head> <title> Upload Daftar Mahasiswa </title>
<link href="style.css" rel="stylesheet" type="text/css">
<script src="jquery.validate.js"></script>
<script>
$(document).ready(function(){
$("#formuploadmhs").validate();
});
</script>
 
<style type="text/css">
label.error {
color: red; padding-left: .5em;
}
</style>
</head>
<body>
<h3 class="labeljudul"> UPLOAD DATA MAHASISWA </h3>
<fieldset>
<legend  class="legend"> Cari File dulu baru di import </legend>
<form id="formuploadmhs" method="post" enctype="multipart/form-data" action="?page=proses_upload_daftar_mhs">
<table>
	<tr>
		<td class="lbltext"> Pilih File Excel [.xls] </td>
		<td class="lbltext"> : </td>
		<td class="isitextbox"><input class="required" name="userfile" type="file" size="45"></td>
	</tr>
	<tr>
		<td> </td>
		<td> </td>
		<td><input name="upload" type="submit" value="Import"></td>
	</tr>
</table>
<table border='1' style='border-collapse:collapse' cellpadding='3' >
	<tr>
		<td colspan="3" ><b> Silahkan Mengupload Data Mahasiswa (.xls / excel 2003) Dengan Format Sebagai Berikut :  </b> </td>
	</tr>
	<tr align='center'>
		<td > NIM </td>
		<td >NAMA MAHASISWA </td>
		<td > ID PRODI </td>
	</tr >	
</table>
</form>
</fieldset>
</body>
</html>