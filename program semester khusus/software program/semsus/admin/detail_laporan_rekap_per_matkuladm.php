<?php 
if(!isset($_SESSION))
{
session_start();
}?>
<html>
<head><title> input mata kuliah</title>
<link href="../style.css" rel="stylesheet" type="text/css">
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script   src="../media/js/jquery.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <style type="text/css">
            @import "../media/css/demo_table_jui.css";
            @import "../media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
        </style>
        
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $('#datatables').dataTable({
                    "sPaginationType":"full_numbers",
                    "aaSorting":[[1, "asc"]],
                    "bJQueryUI":true
                });
            })
            
        </script>
 </head>
<body>
<?php
include ("../koneksi.php");
$idmatkul=$_GET['idmatkul'];
$idprodi=$_GET['idprodi'];
$a=mysql_query("SELECT id_matkul, nm_matkul FROM  tbl_matkul WHERE id_matkul='$idmatkul' AND id_prodi='$idprodi'");
$b=mysql_fetch_array($a);

// mencari nama prodi
$c=mysql_query("SELECT  nm_prodi FROM tbl_prodi WHERE id_prodi='$idprodi'");
$d=mysql_fetch_array($c);

?>
<h3 class="labeljudul" align="center"> REKAP MATA KULIAH YANG AKAN DIAMBIL <br>
PADA SEMESTER KHUSUS THN 2013/2014 </h3>
<table cellpadding="5" cellspacing="2" width="100%">
	<tr>
		<td class="lbltext"> PRODI </td>
		<td class="lbltext"> :</td>
		<td > <input type="text" name="prodi" size="60" value="<?=$d['nm_prodi']?>"> </td>
		<td> </td>
	</tr>
	<tr>
		<td class="lbltext"> Kode, Nama Matkul </td>
		<td class="lbltext"> :</td>
		<td > <input type="text" name="idmatkul"  value="<?=$b['id_matkul']?>">
			 <input type="text" name="nmmatkul" size="50" value="<?=$b['nm_matkul']?>">
		</td>
		<td> </td>
	</tr>		
</table>
<!-- membuat tabel -->
<table id="datatables" class="display" width="10%" >
                <thead>
                    <tr>
                        <th> NO Urut </th>
						 <th> Kode Mata Kuliah </th>
						<th>Nama Mata Kuliah</th>
						
					</tr>
                </thead>
                <tbody>
                    <?php
				$sql="SELECT pil.id_mhs, m.nm_mhs FROM tbl_pilih  AS pil, tbl_mhs AS m
WHERE (pil.id_mhs=m.id_mhs AND pil.id_prodi=m.id_prodi) AND pil.id_matkul='$idmatkul' AND pil.id_prodi='$idprodi'";
				$result = mysql_query($sql) or die(mysql_error());
				$no = 1;
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
							<td align="center"> <?php echo $no ?></td>
							<td><?php echo $row['id_mhs']?></td>
							<td><?php echo $row['nm_mhs']?></td>
				
						</tr>
                        <?php
						$no++;
                    }
                    ?>
                  <tfoot>
                        <tr>
                            <td colspan="10">
                                <a href="excel_laporan_per_prodi_detail.php?idprodi=<?=$idprodi?>&idmatkul=<?=$idmatkul?>">
                                <img width="20" height="20" src="images/excel1.png"></a>
                                <a href="word_laporan_per_prodi_detail.php?idprodi=<?=$idprodi?>&idmatkul=<?=$idmatkul?>">
                                <img width="20" height="20" src="images/word1.jpg"></a>
                                <a href="print_laporan_per_prodi_detail.php?idprodi=<?=$idprodi?>&idmatkul=<?=$idmatkul?>">
                                <img width="20" height="20" src="images/print.jpg"></a>
                                
                            </td>
                        </tr>    
                    </tfoot>

                </tbody>
	</table>	
</body>
</html>