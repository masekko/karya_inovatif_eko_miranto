<?php 
if(!isset($_SESSION))
{
session_start();
}?>
<html>
<head><title> input mata kuliah</title>
<link href="style.css" rel="stylesheet" type="text/css">
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script   src="media/js/jquery.js" type="text/javascript"></script>
        <script src="media/js/jquery.dataTables.js" type="text/javascript"></script>
        <style type="text/css">
            @import "media/css/demo_table_jui.css";
            @import "media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
        </style>
        
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $('#datatables').dataTable({
                    "sPaginationType":"full_numbers",
                    "aaSorting":[[1, "asc"]],
                    "bJQueryUI":true
                });
            })
            
        </script>
 </head>
<body>
<?php
include ("koneksi.php");
$nim=$_SESSION['iduser'];
$idprodi=$_SESSION['idprodi'];
?>
<h3 class="labeljudul" align="center"> REKAP MATA KULIAH YANG AKAN DIAMBIL <br>
PADA SEMESTER KHUSUS </h3>
<table cellpadding="5" cellspacing="2" width="100%">
	<tr>
		<td class="lbltext"> PRODI </td>
		<td class="lbltext"> :</td>
		<td class="isikiri"> <input type="text" name="prodi" size="60" value="<?=$_SESSION['namaprodi']?>"> </td>
		<td> </td>
	</tr>		
</table>
<!-- membuat tabel -->
<table id="datatables" class="display" width="10%" >
                <thead>
                    <tr>
                        <th> NO Urut </th>
						 <th> Kode Mata Kuliah </th>
						<th>Nama Mata Kuliah</th>
						<th> Jumlah Peserta </th>
					</tr>
                </thead>
                <tbody>
                    <?php
				$sql="SELECT pil.id_matkul,m.nm_matkul, COUNT(pil.id_mhs) AS 'jml' FROM tbl_pilih AS pil, tbl_matkul AS m
						WHERE pil.id_matkul=m.id_matkul AND pil.id_prodi=m.id_prodi AND pil.id_prodi='$idprodi' GROUP BY pil.id_prodi,pil.id_matkul";
				$result = mysql_query($sql) or die(mysql_error());
				$no = 1;
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
							<td align="center"> <?php echo $no ?></td>
							<td><?php echo $row['id_matkul']?></td>
							<td><?php echo $row['nm_matkul']?></td>
					<td><a href="?page=detail_laporan_rekap_per_matkul&idmatkul=<?=$row['id_matkul']?>"><?=$row['jml']?></a> </td>
						</tr>
                        <?php
						$no++;
                    }
                    ?>
                     <tfoot>
                        <tr>
                            <td colspan="10">
                                <a href="excel_laporan_per_prodi.php?idprodi=<?=$idprodi?>">
                                <img width="20" height="20" src="admin/images/excel1.png"></a>
                                <a href="word_laporan_per_prodi.php?idprodi=<?=$idprodi?>">
                                <img width="20" height="20" src="admin/images/word1.jpg"></a>
                                <a href="print_laporan_per_prodi.php?idprodi=<?=$idprodi?>">
                                <img width="20" height="20" src="admin/images/print.jpg"></a>
                                
                            </td>
                        </tr>    
                    </tfoot>
                </tbody>
	</table>	
</body>
</html>