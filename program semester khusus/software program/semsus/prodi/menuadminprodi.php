<?php 
if(!isset($_SESSION))
{
session_start();
}
if(!isset($_SESSION['iduser']))
{
	header ("location:index.php");
}
include "../koneksi.php";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>menu admin</title>
<script type="text/javascript" language="javascript"  src="../admin/jquery-1.9.0.js"></script>
<script type="text/javascript">
<!--//---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	//slides the element with class "menu_body" when paragraph with class "menu_head" is clicked 
	$("#firstpane p.menu_head").click(function()
    {
		$(this).css({backgroundImage:"url(../admin/images/down.png)"}).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
       	$(this).siblings().css({backgroundImage:"url(../admin/images/left.png)"});
	});
	//slides the element with class "menu_body" when mouse is over the paragraph
	//$("#secondpane p.menu_head").mouseover(function()
  //  {
	//     $(this).css({backgroundImage:"url(down.png)"}).next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
     //    $(this).siblings().css({backgroundImage:"url(left.png)"});
//	});
});
</script>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div id="page">
  <div id="header" ><img align="left" height="100px" width="500px" src="../images/headerbaru.gif"></div>
  <div id="konten">
    <div id="slidebarkiri">
      <div class="isikiri">
        <div style="float:left" >
          <!--This is the first division of left-->
          <p style="color:#000000; font-size:16px;" align="center"><strong>&nbsp;.:: MENU ::. </strong></p>
          <div id="firstpane"  class="menu_list">
            <!--Code for menu starts here-->
            <p class="menu_head"><img src="../images/cofolder.gif">&nbsp;&nbsp; Master</p>
            	<div  class="menu_body"> 
						<a href="menuadminprodi.php?page=master_mhs_utama">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mahasiswa</a>
						<a href="menuadminprodi.php?page=master_matkul_utama">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mata Kuliah</a> 
						<a href="menuadminprodi.php?page=master_user_utama">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;User</a> 
				</div>		
            <p  class="menu_head"><img src="../images/cofolder.gif">&nbsp;&nbsp; Upload Master</p>
           		<div  class="menu_body"> 
						<a href="menuadminprodi.php?page=upload_form_daftar_mhs">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mahasiswa</a>
						<a href="menuadminprodi.php?page=upload_form_daftar_matkul">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mata Kuliah</a>
						<a href="menuadminprodi.php?page=upload_form_daftar_user">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;User </a>
				</div>
            <p  class="menu_head"><img src="../images/cofolder.gif">&nbsp;&nbsp; Rekap </p>
           		<div  class="menu_body"> 
						<a href="menuadminprodi.php?page=form_lap_per_mhs">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Per Mahasiswa</a> 
						<a href="menuadminprodi.php?page=laporan_rekap_per_prodi_adm">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Per Prodi</a> 
						<a href="menuadminprodi.php?page=form_lap_per_prodirentang">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Per Prodi dg rentang </a> 
				</div>
            <p  class="menu_head"><img src="../images/cofolder.gif">&nbsp;&nbsp; Logout</p>
           		 <div  class="menu_body"> 
				 		<a href="logout.php">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Logout</a> </div>
          		</div>
          <!--Code for menu ends here-->
        </div>
        <!--bagian dari menu -->
      </div>
      <!--isi kiri-->
    </div><!--slidebar kiri -->
    <div id="post">
      <div class="isipost">
	  <?php 

	  $iduser=$_SESSION['iduser'];
	  $idprodi=$_SESSION['idprodi'];
	  
	   
	  // mencari nama prodi
	  $d="SELECT id_prodi, nm_prodi FROM tbl_prodi WHERE id_prodi='$idprodi'";
	  $e=mysql_query($d,$koneksi);
	  $f=mysql_fetch_array($e);

	  if(!isset($_GET['page']))
	  {

	  	echo "<strong> ADMIN PRODI  ".strtoupper($f['nm_prodi'])." <BR> GUNAKAN MENU DISAMPING UNTUK MELIHAT DATA<br></strong>"; //true
	  $_SESSION['idprodi']=$f['id_prodi'];
	  $_SESSION['nmprodi']=strtoupper($f['nm_prodi']);

	  }
	  else
	  {
	  include"$_GET[page].php"; //false
	  }
	  ?>
	  </div> 
      <!--isipost-->
    </div> <!--post-->
   </div> <!-- konten -->
</div> <!-- page -->
</body>
</html>
