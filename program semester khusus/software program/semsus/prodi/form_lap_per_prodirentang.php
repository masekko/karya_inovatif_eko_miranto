<?php 
if(!isset($_SESSION))
{
session_start();
}
$idprodi=$_SESSION['idprodi'];
$nmprodi=$_SESSION['nmprodi'];
include ("../koneksi.php");
?>
<html>
<head>
<link href="style.css" rel="stylesheet" type="text/css">
<script src="../admin/jquery.validate.js"></script>
<script>
$(document).ready(function(){
$("#formrentanglap").validate();
});
</script>
 
<style type="text/css">
label.error {
color: red; padding-left: .5em;
}
</style>
</head>
<body>
<h4 class="labeljudul" align="center"> REKAP MATA KULIAH YANG AKAN DIAMBIL <br>
PRODI <?=$nmprodi?> <br> PADA SEMESTER KHUSUS </h4>
<form  id="formrentanglap" method="post" action="?page=laporan_rentang_proses">
  <table cellpadding="5" cellspacing="5">
    <tr>
        <td class="lbltext"> Masukkan Nilai Awal </td>
        <td class="lbltext">:</td>
        <td class="isitextbox"><input class="required" type="text" name="nilaiawal"></td>
    </tr>    
    <tr>
        <td class="lbltext"> Masukkan Nilai Akhir</td>
        <td class="lbltext"> : </td>
        <td class="isitextbox"><input class="required" type="text" name="nilaiakhir"></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td> <input type="submit" name="submit" value="Proses"></td>
    </tr>
  </table>
</form>
</body>
</html>