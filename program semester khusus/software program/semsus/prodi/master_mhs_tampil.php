<?php 
if(!isset($_SESSION))
{
session_start();
}
include ("../koneksi.php") ?> 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <script   src="../media/js/jquery.js" type="text/javascript"></script>
        <script src="../media/js/jquery.dataTables.js" type="text/javascript"></script>
        <link rel="StyleSheet" href="css/style.css" type="text/css" />
        <style type="text/css">
            @import "../media/css/demo_table_jui.css";
            @import "../media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
        </style>
        
        <style>
            *{
                font-family: arial;
            }
        </style>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $('#datatables').dataTable({
                    "sPaginationType":"full_numbers",
                    "aaSorting":[[2, "desc"]],
                    "bJQueryUI":true
                });
            })
            
        </script>
    </head>
    <body>
        <table id="datatables" class="display">
                <thead>
                    <tr>
                        <th> NO URUT </th>
						<th>NIM</th>
						<th>NAMA MAHASISWA</th>
						<th> DOSEN PRODI </th>
                        <th>EDIT || DEL </th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $idprodi=$_SESSION['idprodi'];
                    $nmprodi=$_SESSION['nmprodi'];
				$sql="SELECT d.id_mhs,d.nm_mhs,p.nm_prodi FROM tbl_mhs  AS d, tbl_prodi AS p WHERE d.id_prodi=p.id_prodi AND d.id_prodi='$idprodi'";
				$result = mysql_query($sql) or die(mysql_error());
				$no = 1;
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
							<td> <?php echo $no ?></td>
							<td><?php echo $row['id_mhs']?></td>
                            <td><?php echo $row['nm_mhs']?></td>
							<td> <?php echo $row['nm_prodi'] ?></td>
                          <?php echo " <td align='center'><a href='javascript:void(0)' onClick=\"edit_form('$row[id_mhs]')\">
																<img title=\"Edit Data\"  src=\"../admin/images/edit.png\"/></a> 
																"."  ||   "."  
										 <a href='javascript:void(0)' onClick=\"delete_data('$row[id_mhs]','$row[nm_mhs]')\">
																<img title =\"Delete Data\" src=\"../admin/images/delete.jpg\" /></a></td> " ?>
                        </tr>
                        <?php
						$no++;
                    }
                    ?>
                </tbody>
            </table>
</body>
</html>
