<?php 
if(!isset($_SESSION))
{
session_start();
}
$idprodi=$_SESSION['idprodi'];
$nmprodi=$_SESSION['nmprodi'];
?>
<html>
<head>
<title> Management data mata kuliah</title>
<script type="text/javascript" src="../admin/jquery-1.9.0.js"></script>
<script>
$(document).ready(function(){
$("#data").load("master_matkul_tampil.php");
});

 // fungsi untuk menampilkan form tambah dan memasukkan data pada id form pd file ini
function tambah_form() {
	$.get('master_matkul_tambah.php',null,function(data) { $('#form').html(data);});
	$('#form').show("slow");
}											

// function untuk menampilkan form edit data dan dimasukkan pada div id='form'
	function edit_form(kode,idprodi) {
		$.get('master_matkul_edit.php?kode='+kode+'&idprodi='+idprodi, null, function(data) {$('#form').html(data);});
		$('#form').show("slow");
	}

function delete_data(kode,idprodi,nmmatkul){
	var pilih=confirm('Yakin akan menghapus record dengan matkul = ' +nmmatkul+'?');
	if(pilih==true){
			$.get('master_matkul_delete.php?idmatkul='+kode+'&idprodi='+idprodi,null,function(data){$('#data').html(data);});
			//$("#data").load("master_prodi_tampil.php");
			}
}
// fungsi untuk proses tambah atau edit data lewat url atu lewat post
function submitForm(url) {
	$("#data").load("master_matkul_tampil.php");
	var thisPost=$('#forms').serialize();
	$.post(url,thisPost,function(data) {$('#form').html(data);});
	$("#data").load("master_matkul_tampil.php");
	return false;
}
</script>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<h3 align="center" class="labeljudul"> MANAJEMEN MATA KULIAH PRODI <?=$nmprodi?> </h3>
<h5 align="left"><a href="javascript:void(0)" onClick="tambah_form()"><img width="30" height="30"  src="../admin/images/new.jpg" title="Add New Data"></a></h5>
<div id="form"></div>
<div id="data"></div>
</body>
</html>