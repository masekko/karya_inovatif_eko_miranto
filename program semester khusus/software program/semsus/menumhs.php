<?php 
if(!isset($_SESSION))
{
session_start();
}
if(!isset($_SESSION['iduser']))
{
	header ("location:index.php");
}
include "koneksi.php";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>menu</title>
<script type="text/javascript" language="javascript"  src="jquery-1.9.0.js"></script>
<script type="text/javascript">
<!--//---------------------------------+
//  Developed by Roshan Bhattarai 
//  Visit http://roshanbh.com.np for this script and more.
//  This notice MUST stay intact for legal use
// --------------------------------->
$(document).ready(function()
{
	//slides the element with class "menu_body" when paragraph with class "menu_head" is clicked 
	$("#firstpane p.menu_head").click(function()
    {
		$(this).css({backgroundImage:"url(down.png)"}).next("div.menu_body").slideToggle(300).siblings("div.menu_body").slideUp("slow");
       	$(this).siblings().css({backgroundImage:"url(left.png)"});
	});
	//slides the element with class "menu_body" when mouse is over the paragraph
	//$("#secondpane p.menu_head").mouseover(function()
  //  {
	//     $(this).css({backgroundImage:"url(down.png)"}).next("div.menu_body").slideDown(500).siblings("div.menu_body").slideUp("slow");
     //    $(this).siblings().css({backgroundImage:"url(left.png)"});
//	});
});
</script>
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="page">
  <div id="header"><img  align="left" height="100px" width="600px" src="images/headerbaru.gif"></div>
  <div id="konten">
    <div id="slidebarkiri">
      <div class="isikiri">
	  <fieldset>
	  <legend> Pilih Menu di bawah ini </legend>
	  <table width="150" cellpadding="10" >
	  	<tr> 
			<td align="center"> <a href="menumhs.php?page=form_tambah_matkul"><img width="40" height="40" src="images/add.PNG"><br>
										Tambah Mata Kuliah</a></td> 
		</tr>
		<tr>
			<td align="center"> <a href="menumhs.php?page=form_hapus_matkul"><img width="40" height="40" src="images/del.PNG"><br>
										Kurangi / Hapus Mata Kuliah</a></td> 
		</tr>	
		<tr>
			<td align="center"> <a href="menumhs.php?page=laporan_rekap_per_mhs"><img width="40" height="40" src="images/look.PNG"><br>
										Lihat MK yg diambil</a></td> 
		</tr>
		<tr>
			<td align="center"> <a href="menumhs.php?page=laporan_rekap_per_prodi"><img width="40" height="40" src="images/detail.PNG"><br>
										Lihat Lap Per Prodi</a></td> 
		</tr>
		<tr>
			<td align="center"> <a href="menumhs.php?page=gantipwd"><img width="40" height="40" src="images/key.PNG"><br>
										Ganti Password</a></td> 
		</tr>
		<tr>
			<td align="center"> <a href="logout.php"><img width="40" height="40" src="images/logout.PNG"><br>
										Logout</a></td> 
		</tr>
	  </table>
	  </fieldset>
       </div><!--isi kiri-->
    </div><!--slidebar kiri -->

    <div id="post">
      <div class="isipost">
	  <?php 
	  $nim=$_SESSION['iduser'];
	  $idprodi=$_SESSION['idprodi'];
	  
	  $a="SELECT nm_mhs FROM tbl_mhs WHERE id_mhs='$nim'";
	  $b=mysql_query($a,$koneksi);
	  $c=mysql_fetch_array($b);
	  
	  // mencari nama prodi
	  $d="SELECT nm_prodi FROM tbl_prodi WHERE id_prodi='$idprodi'";
	  $e=mysql_query($d,$koneksi);
	  $f=mysql_fetch_array($e);
	  
	  if(!isset($_GET['page']))
	  {
	  	echo "Selamat Datang sdr <b> ".strtoupper($c['nm_mhs'])." Mahasiswa Prodi ".strtoupper($f['nm_prodi'])." </b><br>"; //true
		echo " silakan gunakan menu di samping , jangan lupa untuk mengganti password <br>";
		echo " daftar mata kuliah yang diselenggarakan pada semester khusus akan diumumkan kemudian melalui prodi"; 	  
	   $_SESSION['namaprodi']=strtolower($f['nm_prodi']);
	   $_SESSION['namamhs']=$c['nm_mhs'];
	  }
	  else
	  {
	  include"$_GET[page].php"; //false
	  }
	 
	  ?>
	  </div> <!--isipost-->
    </div> <!--post-->
    
  </div> <!-- konten -->
</div> <!-- page -->
</body>
</html>
