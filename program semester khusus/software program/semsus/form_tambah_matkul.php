<?php 
if(!isset($_SESSION))
{
session_start();
}?>
<html>
<head><title> input mata kuliah</title>
<link href="style.css" rel="stylesheet" type="text/css">
       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script   src="media/js/jquery.js" type="text/javascript"></script>
        <script src="media/js/jquery.dataTables.js" type="text/javascript"></script>
        <style type="text/css">
            @import "media/css/demo_table_jui.css";
            @import "media/themes/ui-lightness/jquery-ui-1.8.4.custom.css";
        </style>
        
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function(){
                $('#datatables').dataTable({
                    "sPaginationType":"full_numbers",
                    "aaSorting":[[1, "asc"]],
                    "bJQueryUI":true
                });
            })
            
        </script>
 </head>
<body>
<?php
include ("koneksi.php");
$nim=$_SESSION['iduser'];
$idprodi=$_SESSION['idprodi'];
$a=mysql_query("SELECT id_mhs,nm_mhs,id_prodi FROM tbl_mhs WHERE id_mhs='$nim'",$koneksi);
$b=mysql_fetch_array($a);
?>
<h3 class="labeljudul" align="center"> FORM TAMBAH MATA KULIAH YANG AKAN DIAMBIL <br>
PADA SEMESTER KHUSUS </h3>
<form action="?page=proses" method="post">
<table cellpadding="5" cellspacing="2" width="100%">
	<tr>
		<td class="lbltext"> NIM </td>
		<td class="lbltext"> :</td>
		<td class="isikiri"> <input type="text" name="nim" value="<?=$nim?>"> </td>
		<td> </td>
		<td class="lbltext"> NAMA </td>
		<td class="lbltext"> :</td>
		<td class="isikiri"> <input type="text" name="nama"  size="35" value="<?=$b['nm_mhs']?>"> </td>
		<td align="right"><input type="submit" name="submit" value="Proses"> </td>
	</tr>
	<tr>
		<td class="lbltext"> PRODI </td>
		<td class="lbltext"> :</td>
		<td class="isikiri"> <input type="text" name="prodi" size="35" value="<?=$_SESSION['namaprodi']?>"> </td>
		<td> </td>
	</tr>		
</table>
<!-- membuat tabel -->
<table id="datatables" class="display" width="10%" >
                <thead>
                    <tr>
                        <th> NO Urut </th>
						 <th> Kode Mata Kuliah </th>
						<th>Nama Mata Kuliah</th>
						<th> Pilih Mata Kuliah </th>
					</tr>
                </thead>
                <tbody>
                    <?php
				$sql="SELECT  id_matkul, nm_matkul FROM tbl_matkul WHERE id_prodi='$idprodi'";
				$result = mysql_query($sql) or die(mysql_error());
				$no = 1;
                    while ($row = mysql_fetch_array($result)) {
                        ?>
                        <tr>
							<td align="center"> <?php echo $no ?></td>
							<td><?php echo $row['id_matkul']?></td>
							<td><?php echo $row['nm_matkul']?></td>
							<td><input type="checkbox" name="matkul<?=$no?>" value="<?=$row['id_matkul']?>"></td>                        </tr>
                        <?php
						$no++;
                    }
                    ?>
                </tbody>
	</table>
	<div align="right"> <input type="submit" name="submit" value="Proses"></div>
	<input type="hidden" name="jmlmatkul" value="<?=$no-1?>">
</form>
</body>
</html>